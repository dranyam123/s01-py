name = "Maynard Escalante"
age = 33
occupation = "Full Stack Web Developer"
movie = "Avatar: Way of the Water"
rating = 9.99

print(f"I am {name}, and I am {age} years old, I work as a {occupation}, and my rating for {movie} is {rating}%")

num1, num2, num3 = 25, 50, 100

product = num1 * num2
isLessThan = num1<num3
value = num3 + num2

print(product)
print(isLessThan)
print(value)